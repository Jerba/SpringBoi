﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace SpringBoi
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public static List<GameObject> gameObjects = new List<GameObject>();
        public static List<GameObject> level1 = new List<GameObject>();
        public static List<GameObject> level2 = new List<GameObject>();
        public static List<GameObject> level3 = new List<GameObject>();
        public static List<GameObject> level4 = new List<GameObject>();
        public static List<GameObject> level5 = new List<GameObject>();
        public static List<GameObject> level6 = new List<GameObject>();
        public static List<GameObject> level7 = new List<GameObject>();
        public static List<GameObject> level8 = new List<GameObject>();
        public static List<GameObject> level9 = new List<GameObject>();
        public static Texture2D pixel;
        public static SpriteFont font;
        public static float halt;

        private static List<GameObject> newObjects = new List<GameObject>();
        int level = 0;
        public void ChangeLevel()
        {
            gameObjects.Clear();
            switch (level)
            {
                default:
                case 1: gameObjects = level1.GetRange(0, level1.Count); break;
                case 2: gameObjects = level2.GetRange(0, level2.Count); break;
                case 3: gameObjects = level3.GetRange(0, level3.Count); break;
                case 4: gameObjects = level4.GetRange(0, level4.Count); break;
                case 5: gameObjects = level5.GetRange(0, level5.Count); break;
                case 6: gameObjects = level6.GetRange(0, level6.Count); break;
                case 7: gameObjects = level7.GetRange(0, level7.Count); break;
                case 8: gameObjects = level8.GetRange(0, level8.Count); break;
                case 9: gameObjects = level9.GetRange(0, level9.Count); break;
            }
            foreach (GameObject g in gameObjects) { g.Reset(); }
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth   = 800;
            graphics.PreferredBackBufferHeight  = 600;
            Window.Title = "Spring Boi by Jerba";
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            pixel = new Texture2D(GraphicsDevice, 1, 1);
            Color[] color = { Color.White };
            pixel.SetData<Color>(color, 0, 1);

            Texture2D spring    = Content.Load<Texture2D>("spring");
            Texture2D grass     = Content.Load<Texture2D>("grass");
            Texture2D grassbody = Content.Load<Texture2D>("grassbody");
            Texture2D kid       = Content.Load<Texture2D>("kid");
            Audio.jump          = Content.Load<SoundEffect>("jump");
            Audio.kiddo         = Content.Load<SoundEffect>("kiddo");
            Audio.die           = Content.Load<SoundEffect>("die");
            Song music          = Content.Load<Song>("thefourseasons_spring");

            MediaPlayer.Play(music);
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume      = 0.334f;

            font = Content.Load<SpriteFont>("font");

            level1.Add(new Ground(grass, grassbody, new Vector2(0, 500), new Vector2(800, 100)));
            level1.Add(new Ground(grass, grassbody, new Vector2(0, 470), new Vector2(320, 30)));
            level1.Add(new Ground(grass, grassbody, new Vector2(0, 440), new Vector2(240, 30)));
            level1.Add(new Ground(grass, grassbody, new Vector2(0, 410), new Vector2(180, 30)));
            level1.Add(new Ground(grass, grassbody, new Vector2(0, 360), new Vector2(128, 50)));
            level1.Add(new Ground(grass, grassbody, new Vector2(0, 310), new Vector2(64, 50)));
            level1.Add(new Ground(grass, grassbody, new Vector2(400, 460), new Vector2(300, 40)));
            level1.Add(new Ground(grass, grassbody, new Vector2(500, 380), new Vector2(150, 80)));
            level1.Add(new Kid(kid, new Vector2(32 - 10, 310 - 32)));
            level1.Add(new Kid(kid, new Vector2(740, 500 - 32)));
            level1.Add(new Player(spring, new Vector2(400, 360)));

            level2.Add(new Ground(grass, grassbody, new Vector2(0, 500), new Vector2(800, 100)));
            level2.Add(new Hazard(new Vector2(500, 420), new Vector2(16, 64), 5.0f));
            level2.Add(new Hazard(new Vector2(260, 230), new Vector2(20, 160), 0.334f));
            level2.Add(new Kid(kid, new Vector2(64 - 10, 500 - 32)));
            level2.Add(new Kid(kid, new Vector2(720, 500 - 32)));
            level2.Add(new Player(spring, new Vector2(400, 380)));

            level3.Add(new Ground(grass, grassbody, new Vector2(0, 500), new Vector2(480, 100)));
            level3.Add(new Ground(grass, grassbody, new Vector2(600, 420), new Vector2(200, 200)));
            level3.Add(new Ground(grass, grassbody, new Vector2(0, 160), new Vector2(112, 64)));
            level3.Add(new Ground(grass, grassbody, new Vector2(340, 120), new Vector2(64, 160)));
            level3.Add(new Ground(grass, grassbody, new Vector2(500, 180), new Vector2(160, 64)));
            level3.Add(new Ground(grass, grassbody, new Vector2(180, 460), new Vector2(80, 40)));
            level3.Add(new Hazard(new Vector2(220, 110), new Vector2(16, 160), 1.5f));
            level3.Add(new Hazard(new Vector2(580, 100), new Vector2(8, 64), 10f));
            level3.Add(new Hazard(new Vector2(48, 410), new Vector2(8, 64), -5f));
            level3.Add(new Kid(kid, new Vector2(340 + 16, 120 - 32)));
            level3.Add(new Kid(kid, new Vector2(128, 500 - 32)));
            level3.Add(new Kid(kid, new Vector2(740, 420 - 32)));
            level3.Add(new Player(spring, new Vector2(64, 64)));

            level4.Add(new Ground(grass, grassbody, new Vector2(0, 220), new Vector2(96, 388)));
            level4.Add(new Ground(grass, grassbody, new Vector2(96, 330), new Vector2(96, 278)));
            level4.Add(new Ground(grass, grassbody, new Vector2(696, 500), new Vector2(128, 278)));
            level4.Add(new Ground(grass, grassbody, new Vector2(610, 180), new Vector2(96, 64), new Vector2(96, 180), 2.0f, 180));
            level4.Add(new Ground(grass, grassbody, new Vector2(192, 500), new Vector2(128, 96), new Vector2(568, 500), 2.0f, 180));
            level4.Add(new Hazard(new Vector2(660, 320), new Vector2(8, 64), 10f));
            level4.Add(new Kid(kid, new Vector2(740, 500 - 32)));
            level4.Add(new Kid(kid, new Vector2(40, 220 - 32)));
            level4.Add(new Player(spring, new Vector2(650, 100)));

            level5.Add(new Ground(grass, grassbody, new Vector2(400 - 48, 220), new Vector2(96, 388)));
            level5.Add(new Ground(grass, grassbody, new Vector2(0, 500), new Vector2(800, 100)));
            level5.Add(new Ground(grass, grassbody, new Vector2(0, -16), new Vector2(96, 616)));
            level5.Add(new Ground(grass, grassbody, new Vector2(96, 300), new Vector2(96, 196)));
            level5.Add(new Ground(grass, grassbody, new Vector2(600, 280), new Vector2(200, 48)));
            level5.Add(new Kid(kid, new Vector2(260, 500 - 32)));
            level5.Add(new Kid(kid, new Vector2(390, 220 - 32)));
            level5.Add(new Player(spring, new Vector2(700, 420)));
            level5.Add(new Hazard(new Vector2(240, 240), new Vector2(64, 64), 5.0f, new Vector2(240, 100), 2.0f, 180));

            level6.Add(new Ground(grass, grassbody, new Vector2(0, 500), new Vector2(800, 100)));
            level6.Add(new Hazard(new Vector2(0, 360), new Vector2(320, 16), 0.0f));
            level6.Add(new Hazard(new Vector2(775, 360), new Vector2(320, 16), 0.0f));
            level6.Add(new Hazard(new Vector2(450, 430), new Vector2(16, 70), 0.0f));
            level6.Add(new Hazard(new Vector2(620, 430), new Vector2(16, 70), 0.0f));
            level6.Add(new Hazard(new Vector2(285, 285), new Vector2(220, 16), 0.0f, -45.0f));
            level6.Add(new Hazard(new Vector2(467, 209), new Vector2(160, 16), 0.0f));
            level6.Add(new Hazard(new Vector2(590, 285), new Vector2(220, 16), 0.0f, 45.0f));
            level6.Add(new Kid(kid, new Vector2(760, 500 - 32)));
            level6.Add(new Player(spring, new Vector2(64, 420)));

            level = 1;
            ChangeLevel();
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            Input.UpdateInput();

            if (Input.EscPressed())
                Exit();

            foreach (GameObject g in gameObjects)
            {
                g.Update();
            }

            bool changeLevel = true;
            foreach (Kid kid in gameObjects.OfType<Kid>())
            {
                if (!kid.saved)
                {
                    changeLevel = false;
                    break;
                }
            }

            if (Input.LeftShiftHeld())
            {
                if      (Input.Key1Pressed()) { level = 0; changeLevel = true; }
                else if (Input.Key2Pressed()) { level = 1; changeLevel = true; }
                else if (Input.Key3Pressed()) { level = 2; changeLevel = true; }
                else if (Input.Key4Pressed()) { level = 3; changeLevel = true; }
                else if (Input.Key5Pressed()) { level = 4; changeLevel = true; }
                else if (Input.Key6Pressed()) { level = 5; changeLevel = true; }
                else if (Input.Key7Pressed()) { level = 6; changeLevel = true; }
                else if (Input.Key8Pressed()) { level = 7; changeLevel = true; }
                else if (Input.Key9Pressed()) { level = 8; changeLevel = true; }
                else
                {
                    if (Input.MouseLeftHeld())
                    {
                        foreach (Player p in gameObjects.OfType<Player>())
                        {
                            p.position  = Input.MousePosition();
                            p.velocity  = Vector2.Zero;
                            p.angle     = 0;
                        }
                    }
                }
            }

            if (changeLevel)
            {
                level++;
                if (level > 6)
                    level = 1;
                ChangeLevel();
            }

            if (halt > 0) --halt;

            if (newObjects.Count > 0)
            {
                gameObjects.AddRange(newObjects);
                newObjects.Clear();
            }

            if (Input.F5Pressed())
                graphics.ToggleFullScreen();

            Input.UpdateInputLast();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DeepSkyBlue);

            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, SamplerState.LinearWrap);
            foreach (GameObject g in gameObjects)
                g.Draw(spriteBatch);

            string lvltext = "Level: " + level.ToString();
            string fails   = string.Format("Fails: {0:00}", Player.fails);

            DrawText(lvltext, new Vector2(20, 20), 2, 0.5f);
            DrawText(fails,   new Vector2(630, 20), 2, 0.5f);
            if (halt > 0)
            {
                int number  = (int)(halt / 40) + 1;
                string wait = number.ToString();
                DrawText(wait, new Vector2(400, 150) - font.MeasureString(wait) * 0.5f, 3, 1.0f);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        public static void ShowText(Vector2 pos, string txt, Color col1, Color col2)
        {
            newObjects.Add(new ParticleText(pos, new Vector2(0, -2), txt, 0.5f, col1, col2, 30));
        }

        public void DrawText(string text, Vector2 pos, int outline, float scale = 1.0f)
        {
            spriteBatch.DrawString(font, text, pos + new Vector2(outline, 0), Color.Black, 0, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
            spriteBatch.DrawString(font, text, pos + new Vector2(-outline, 0), Color.Black, 0, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
            spriteBatch.DrawString(font, text, pos + new Vector2(0, outline), Color.Black, 0, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
            spriteBatch.DrawString(font, text, pos + new Vector2(0, -outline), Color.Black, 0, Vector2.Zero, scale, SpriteEffects.None, 0.9f);

            spriteBatch.DrawString(font, text, pos, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.91f);
        }
    }
}
