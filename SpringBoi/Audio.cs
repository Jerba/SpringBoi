﻿using Microsoft.Xna.Framework.Audio;

namespace SpringBoi
{
    public static class Audio
    {
        public static SoundEffect jump, die, kiddo;
        public static void PlaySound(string sound, float volume, float pitch)
        {
            volume = RNG.Between(volume - 0.05f, volume + 0.05f);
            pitch  = RNG.Between(pitch - 0.05f, pitch + 0.05f);
            if (pitch > 1) pitch = 1.0f; else if (pitch < 0) pitch = 0;

            switch (sound.ToLower())
            {
                case "jump":    jump.Play(volume,  pitch, 0); break;
                case "die":     die.Play(volume,   pitch, 0); break;
                case "kiddo":   kiddo.Play(volume, pitch, 0); break;
            }
        }
    }
}
