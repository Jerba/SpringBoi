﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpringBoi
{
    public class ParticleText : GameObject
    {
        public Vector2  velocity;
        public string   text;
        public Color    mainColor;
        public Color    borderColor;
        public int      life;
        public int      timeAlive;
        public float    scale = 1;

        public ParticleText(Vector2 pos, Vector2 vel, string txt, float size, Color txtcol, Color txtbordercol, int time)
        {
            position    = pos;
            velocity    = vel;
            text        = txt;
            mainColor   = txtcol;
            borderColor = txtbordercol;
            life        = time;
            scale       = size;
            timeAlive   = 0;
        }

        public override void Reset()
        {

        }

        public override void Update()
        {
            if (timeAlive > life)
                return;
            ++timeAlive;
            position += velocity;
        }

        public override void Draw(SpriteBatch batch)
        {
            if (timeAlive > life)
                return;

            float m = 1.0f - ((float)timeAlive / (float)life);
            if (m > 1.0f) m = 1.0f; else if (m < 0) return;

            Color border    = borderColor;
            border.A        =  (m < 1.0f) ? (byte)(borderColor.A * m * 0.6f) : (byte)(borderColor.A * m);
            Color main      = mainColor;
            main.A          = (byte)(mainColor.A * m);

            batch.DrawString(Game1.font, text, position + new Vector2(1, 0), border, 0, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
            batch.DrawString(Game1.font, text, position + new Vector2(-1, 0), border, 0, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
            batch.DrawString(Game1.font, text, position + new Vector2(0, 1), border, 0, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
            batch.DrawString(Game1.font, text, position + new Vector2(0, -1), border, 0, Vector2.Zero, scale, SpriteEffects.None, 0.9f);

            batch.DrawString(Game1.font, text, position, main, 0, Vector2.Zero, scale, SpriteEffects.None, 0.91f);
        }
    }
}
