﻿using System;
namespace SpringBoi
{
    public static class RNG
    {
        static Random random = new Random();

        public static float Between(float min, float max)
            { return (float)(min + random.NextDouble() * (max - min)); }
    }
}
