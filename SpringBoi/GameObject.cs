﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpringBoi
{
    public abstract class GameObject
    {
        public Vector2 position;
        public abstract void Update();
        public abstract void Draw(SpriteBatch batch);
        public abstract void Reset();
    }
}
