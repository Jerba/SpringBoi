﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpringBoi
{
    public class Player : GameObject
    {
        public Texture2D        texture;
        public Vector2          size;
        public Vector2          startPos;
        public float            angle;
        public float            angleRad;
        public float            turnSpeed;
        public RotatedRectangle collider;
        public RotatedRectangle head;
        public Color            color;
        public Vector2          velocity;
        public float            gravity  = 1.0f;
        public bool             grounded, groundedM;
        public float            heightScale;
        public float            heightSpeed;
        public bool             jumping;
        public bool             facingRight;
        public bool             dead;
        public int              deadTimer;
        public int              noCollision;
        public static int       fails = 0;
        public SheetAnimation   animation;
        public Ground           lastGround;

        public Player(Texture2D tex, Vector2 pos)
        {
            startPos    = pos;
            position    = pos;
            texture     = Game1.pixel;
            size        = new Vector2(32, 70);
            velocity    = new Vector2(0, 9.81f);
            angle       = 0;
            angleRad    = 0;
            turnSpeed   = 3;
            heightScale = 1.0f;
            heightSpeed = 0.1f;
            jumping     = false;
            grounded    = false;
            groundedM   = false;
            dead        = false;
            facingRight = true;
            color       = Color.White;
            animation   = new SheetAnimation(tex, 36, 70, 6, 4); animation.Stop();
            collider    = new RotatedRectangle(pos, size);
            head        = new RotatedRectangle(pos - new Vector2(2), new Vector2(size.X + 4, 48));
            head.origin = collider.origin + new Vector2(2);
        }

        public override void Reset()
        {
            position    = startPos;
            velocity    = new Vector2(0, 9.81f);
            angle       = 0;
            angleRad    = 0;
            turnSpeed   = 3;
            heightScale = 1.0f;
            heightSpeed = 0.1f;
            facingRight = true;
            lastGround  = null;
            jumping     = false;
            grounded    = false;
            groundedM   = false;
            dead        = false;
            deadTimer   = 0;
            noCollision = 0;
            color       = Color.White;
            Game1.halt  = 120;

            foreach (GameObject g in Game1.gameObjects)
            {
                if (g != this)
                    g.Reset();
            }

            collider.position   = position;
            collider.angle      = angleRad;
            head.position       = position - new Vector2(2);
            head.angle          = angleRad;

            collider.Update();
            head.Update();
        }

        public override void Update()
        {
            if (Game1.halt > 0) return;
            if (!dead) PlayerInput();
            Movement();
            Collisions();
            Animation();
            Dead();
        }

        public void PlayerInput()
        {
            if (Input.LeftHeld())
            {
                angle -= turnSpeed;
                if (angle < 0) angle = 360;
            }
            if (Input.RightHeld())
            {
                angle += turnSpeed;
                if (angle > 360) angle = 0;
            }
        }

        public void Movement()
        {
            float angleMultiplier = 0.0f;
            if (angleRad > 0 && angleRad <= Math.PI / 2.0f)
            {
                facingRight = true;
                angleMultiplier = (float)(angleRad / (Math.PI / 2.0f));
            }
            else if (angleRad >= Math.PI * 1.5f)
            {
                facingRight = false;
                angleMultiplier = 1.0f - (float)((angleRad - (float)Math.PI * 1.5f) / (Math.PI / 2.0f));
            }

            if (facingRight) angle += angleMultiplier * 3;
            else             angle -= angleMultiplier * 3;

            angleRad = MathHelper.ToRadians(angle);
            if (groundedM && lastGround != null) { velocity += lastGround.lastMove; }
            position += velocity;

            if (!grounded) velocity.Y += gravity;
            if (jumping)
            {
                heightScale -= heightSpeed;

                if (heightScale < 0.25f)
                {
                    Audio.PlaySound("jump", 0.95f, 0.95f);
                    Vector2 jumpDir = new Vector2((float)Math.Cos(angleRad - Math.PI / 2.0f), (float)Math.Sin(angleRad - Math.PI / 2.0f));
                    jumpDir *= 5;
                    jumpDir.X *= 2;
                    float horizMultiplier = angleMultiplier;

                    if (angleRad > 0 && angleRad <= Math.PI / 2.0f)
                    {
                        horizMultiplier *= 3;
                    }
                    else if (angleRad >= Math.PI * 1.5f)
                    {
                        horizMultiplier *= 3;
                    }

                    velocity.X += jumpDir.X;
                    velocity.X *= horizMultiplier;
                    velocity.Y = jumpDir.Y;
                    if (Input.DownHeld()) velocity.Y -= 4;
                    else velocity.Y -= Input.JumpHeld() ? 12 : 7;
                    if (groundedM && lastGround != null) { velocity += lastGround.lastMove; }

                    grounded    = false;
                    groundedM   = false;
                    jumping = false;
                    noCollision = 6;
                }
            }
            else
            {
                if (heightScale < 1.0f)
                {
                    heightScale += 0.2f;
                    if (heightScale > 1.0f)
                        heightScale = 1.0f;
                }
            }
        }

        public void Collisions()
        {
            if (noCollision > 0) --noCollision;
            int steps       = (int)(velocity.Length() * 2) + 1;
            Vector2 step    = velocity / steps;
            collider.angle  = angleRad;
            head.angle      = angleRad;
            bool collided   = false;

            for (int i = 0; i <= steps; ++i)
            {
                if (dead) break;
                collider.position += step;
                collider.Update();
                head.position = collider.position - new Vector2(2);
                head.Update();

                foreach (Kid kid in Game1.gameObjects.OfType<Kid>())
                {
                    if (kid.saved) continue;
                    if (collider.Collision(kid.collider))
                    {
                        kid.saved = true;
                        Game1.ShowText(kid.position, RNG.Between(0, 1) > 0.5f ? ":)" : ":D",
                            Color.Yellow, Color.Orange);
                        Audio.PlaySound("kiddo", 0.95f, 0.95f);
                    }
                }

                foreach (Ground ground in Game1.gameObjects.OfType<Ground>())
                {
                    if (noCollision > 0)
                        continue;

                    if (head.Collision(ground.collider))
                    {
                        Die();
                        break;
                    }

                    if (collider.Collision(ground.collider))
                    {
                        heightSpeed = velocity.Y * 0.008f;
                        if (heightSpeed < 0.1f) heightSpeed = 0.1f;
                        velocity.X *= 0.5f;
                        velocity.Y  = 0;
                        collided    = true;
                        jumping     = true;
                        grounded    = true;
                        groundedM   = ground.moving;
                        lastGround  = ground;
                        position    = collider.position;
                        break;
                    }
                }

                foreach (Hazard hazard in Game1.gameObjects.OfType<Hazard>())
                {
                    if (collider.Collision(hazard.collider) ||
                        head.Collision(hazard.collider))
                    {
                        velocity.Y  = 0;
                        collided    = true;
                        position    = collider.position;
                        Die();
                        break;
                    }
                }
            }

            if (collided)
                position = collider.position;
            else
            {
                collider.position = position;
                collider.Update();
                head.position = collider.position - new Vector2(2);
                head.Update();
            }
        }

        public void Animation()
        {
            animation.Update();
            if (heightScale < 0.4f)
                animation.SetFrame(5);
            else if (heightScale < 0.5f)
                animation.SetFrame(4);
            else if (heightScale < 0.6f)
                animation.SetFrame(3);
            else if (heightScale < 0.7f)
                animation.SetFrame(2);
            else if (heightScale < 0.8f)
                animation.SetFrame(1);
            else
                animation.SetFrame(0);
        }

        public void Dead()
        {
            if (dead)
            {
                deadTimer--;
                if (deadTimer <= 0)
                    Reset();
                return;
            }

            if (position.X > 1000 || position.X < -200 ||
                position.Y > 800  || position.Y < -200)
            {
                Die(true);
            }
        }

        public void Die(bool oob = false)
        {
            if (dead) return;
            jumping = grounded = groundedM = false;
            lastGround  = null;
            dead        = true;
            deadTimer   = noCollision = 60;

            if (!oob)
            {
                velocity.X  *= -1;
                velocity.Y  -= 15;
            }

            ++fails;
            Audio.PlaySound("die", 0.95f, 0.95f);
            Game1.ShowText(position, ":(", Color.Red, Color.DarkRed);
        }

        public override void Draw(SpriteBatch batch)
        {
            Rectangle playerRect = collider.rect;
            Vector2 drawPos = new Vector2((int)collider.position.X + collider.origin.X, (int)collider.position.Y + collider.origin.Y);
            //batch.Draw(texture, drawPos, playerRect, color, collider.angle, collider.origin, 1, SpriteEffects.None, 1);
            //batch.Draw(texture, new Vector2((int)head.position.X + head.origin.X, (int)head.position.Y + head.origin.Y),
            //            head.rect, Color.Green, head.angle, head.origin, 1, SpriteEffects.None, 1);
            animation.Draw(batch, drawPos - new Vector2(2, -2), collider.origin, angleRad, 0);
        }
    }
}
