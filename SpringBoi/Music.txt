The Four Seasons (Vivaldi)
Spring Mvt 1 Allegro by John Harrison with the Wichita State University Chamber Players
License: Attribution-ShareAlike 3.0 International License.

Downloaded from
https://freemusicarchive.org/music/John_Harrison_with_the_Wichita_State_University_Chamber_Players/The_Four_Seasons_Vivaldi/01_-_Vivaldi_Spring_mvt_1_Allegro_-_John_Harrison_violin