﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SpringBoi
{
    public static class Input
    {
        public static KeyboardState keyState;
        public static KeyboardState keyStateLast;
        public static MouseState    mouseState;

        public static void UpdateInput()
        {
            keyState    = Keyboard.GetState();
            mouseState  = Mouse.GetState();
        }

        public static void UpdateInputLast()
        {
            keyStateLast = keyState;
        }

        public static bool JumpHeld()
        {
            if (keyState.IsKeyDown(Keys.W))
                return true;
            return ArrowUpHeld();
        }

        public static bool JumpPressed()
        {
            if (keyState.IsKeyDown(Keys.W) &&
                keyStateLast.IsKeyUp(Keys.W))
                return true;
            return false;
        }

        public static bool LeftHeld()
        {
            if (keyState.IsKeyDown(Keys.A))
                return true;
            return ArrowLeftHeld();
        }

        public static bool LeftPressed()
        {
            if (keyState.IsKeyDown(Keys.A) &&
                keyStateLast.IsKeyUp(Keys.A))
                return true;
            return false;
        }

        public static bool RightHeld()
        {
            if (keyState.IsKeyDown(Keys.D))
                return true;
            return ArrowRightHeld();
        }

        public static bool RightPressed()
        {
            if (keyState.IsKeyDown(Keys.D) &&
                keyStateLast.IsKeyUp(Keys.D))
                return true;
            return false;
        }

        public static bool LeftShiftHeld()
        {
            return keyState.IsKeyDown(Keys.LeftShift);
        }

        public static bool Key1Pressed()
        {
            if (keyState.IsKeyDown(Keys.D1) &&
                keyStateLast.IsKeyUp(Keys.D1))
                return true;
            return false;
        }

        public static bool Key2Pressed()
        {
            if (keyState.IsKeyDown(Keys.D2) &&
                keyStateLast.IsKeyUp(Keys.D2))
                return true;
            return false;
        }

        public static bool Key3Pressed()
        {
            if (keyState.IsKeyDown(Keys.D3) &&
                keyStateLast.IsKeyUp(Keys.D3))
                return true;
            return false;
        }

        public static bool Key4Pressed()
        {
            if (keyState.IsKeyDown(Keys.D4) &&
                keyStateLast.IsKeyUp(Keys.D4))
                return true;
            return false;
        }

        public static bool Key5Pressed()
        {
            if (keyState.IsKeyDown(Keys.D5) &&
                keyStateLast.IsKeyUp(Keys.D5))
                return true;
            return false;
        }

        public static bool Key6Pressed()
        {
            if (keyState.IsKeyDown(Keys.D6) &&
                keyStateLast.IsKeyUp(Keys.D6))
                return true;
            return false;
        }

        public static bool Key7Pressed()
        {
            if (keyState.IsKeyDown(Keys.D7) &&
                keyStateLast.IsKeyUp(Keys.D7))
                return true;
            return false;
        }

        public static bool Key8Pressed()
        {
            if (keyState.IsKeyDown(Keys.D8) &&
                keyStateLast.IsKeyUp(Keys.D8))
                return true;
            return false;
        }

        public static bool Key9Pressed()
        {
            if (keyState.IsKeyDown(Keys.D9) &&
                keyStateLast.IsKeyUp(Keys.D9))
                return true;
            return false;
        }

        public static bool DownHeld()
        {
            if (keyState.IsKeyDown(Keys.S))
                return true;
            return ArrowDownHeld();
        }

        public static bool DownPressed()
        {
            if (keyState.IsKeyDown(Keys.S) &&
                keyStateLast.IsKeyUp(Keys.S))
                return true;
            return false;
        }

        public static bool ArrowUpHeld()
        {
            return keyState.IsKeyDown(Keys.Up);
        }

        public static bool ArrowDownHeld()
        {
            return keyState.IsKeyDown(Keys.Down);
        }

        public static bool ArrowLeftHeld()
        {
            return keyState.IsKeyDown(Keys.Left);
        }

        public static bool ArrowRightHeld()
        {
            return keyState.IsKeyDown(Keys.Right);
        }

        public static bool F5Pressed()
        {
            if (keyState.IsKeyDown(Keys.F5) &&
                keyStateLast.IsKeyUp(Keys.F5))
                return true;
            return false;
        }

        public static bool EscPressed()
        {
            if (keyState.IsKeyDown(Keys.Escape) &&
                keyStateLast.IsKeyUp(Keys.Escape))
                return true;
            return false;
        }

        public static Vector2 MousePosition()
        {
            return mouseState.Position.ToVector2();
        }

        public static bool MouseLeftHeld()
        {
            return mouseState.LeftButton == ButtonState.Pressed;
        }
    }
}
