﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpringBoi
{
    public class Hazard : GameObject
    {
        public Texture2D        texture;
        public Vector2          size;
        public Vector2          startPos, endPos, moveStart, moveGoal;
        public int              stopTime, stopTimer;
        public float            moveSpeed;
        public float            angle;
        public float            angleRad;
        public float            turnSpeed;
        public RotatedRectangle collider;
        public Color            color;
        public bool             moving;

        public Hazard(Vector2 pos, Vector2 sz, float speed, Vector2 end, float move, int stop, float rot = 0)
        {
            startPos    = pos;
            position    = pos;
            endPos      = end;
            moveStart   = startPos;
            moveGoal    = endPos;
            texture     = Game1.pixel;
            size        = sz;
            angle       = rot;
            angleRad    = MathHelper.ToRadians(rot);
            turnSpeed   = speed;
            moveSpeed   = move;
            stopTimer   = stop;
            stopTime    = stop;
            color       = Color.DarkRed;
            moving      = true;

            collider            = new RotatedRectangle(pos, size);
            collider.angle      = angleRad;
            collider.position   = position;
            collider.Update();
        }

        public Hazard(Vector2 pos, Vector2 sz, float speed, float rot = 0)
        {
            position    = pos;
            texture     = Game1.pixel;
            size        = sz;
            angle       = rot;
            angleRad    = MathHelper.ToRadians(rot);
            turnSpeed   = speed;
            color       = Color.DarkRed;
            moving      = false;

            collider            = new RotatedRectangle(pos, size);
            collider.angle      = angleRad;
            collider.position   = position;
            collider.Update();
        }

        public override void Update()
        {
            angle += turnSpeed;
            if (angle > 360) angle = 0;
            angleRad = MathHelper.ToRadians(angle);

            if (moving) { Move(); }

            collider.position   = position;
            collider.angle      = angleRad;
            collider.Update();
        }

        public void Move()
        {
            if (stopTimer > 0) --stopTimer;
            else
            {
                Vector2 delta = moveGoal - moveStart;

                if (delta != Vector2.Zero) delta.Normalize();
                delta *= moveSpeed;
                position += delta;

                float dist = Vector2.Distance(moveStart, moveGoal);
                float current = Vector2.Distance(position, moveGoal);
                float percentage = 1 - (current / dist);
                float accuracy = 1;

                if ((Math.Abs(delta.X) > 0.6f && Math.Abs(delta.X) < 0.8f) || (Math.Abs(delta.Y) > 0.6f && Math.Abs(delta.Y) < 0.8f))
                { accuracy = .99f; }

                if (percentage >= accuracy)
                {
                    position = moveGoal;
                    if (moveGoal == endPos)
                    {
                        moveStart = endPos;
                        moveGoal  = startPos;
                    }
                    else
                    {
                        moveStart = startPos;
                        moveGoal  = endPos;
                    }
                    stopTimer = stopTime;
                }
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            Rectangle outline = collider.rect;
            outline.X       -= 2;
            outline.Y       -= 2;
            outline.Width   += 4;
            outline.Height  += 4;
            batch.Draw(texture, new Vector2((int)collider.position.X + collider.origin.X,
                        (int)collider.position.Y + collider.origin.Y),
                        outline, Color.Black, collider.angle, collider.origin + new Vector2(2),
                        1, SpriteEffects.None, 0.01f);
            batch.Draw(texture, new Vector2((int)collider.position.X + collider.origin.X,
                        (int)collider.position.Y + collider.origin.Y),
                        collider.rect, color, collider.angle, collider.origin,
                        1, SpriteEffects.None, 0.02f);
        }

        public override void Reset()
        {
            if (moving)
            {
                position    = startPos;
                moveStart   = startPos;
                moveGoal    = endPos;
                stopTimer   = stopTime;
            }
        }
    }
}
