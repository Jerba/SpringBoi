﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpringBoi
{
    public class Kid : GameObject
    {
        public Vector2          size;
        public RotatedRectangle collider;
        public Color            color;
        public SheetAnimation   animation;
        public bool             saved;

        public Kid(Texture2D tex, Vector2 pos)
        {
            position    = pos;
            color       = Color.White;
            size        = new Vector2(20, 32);
            animation   = new SheetAnimation(tex, 28, 36, 4, 6);
            collider    = new RotatedRectangle(pos, size);
            saved       = false;

            collider.position   = pos;
            collider.angle      = 0;
            collider.Update();
        }

        public override void Reset()
        {
            saved = false;
        }

        public override void Update()
        {
            collider.position = position;
            collider.Update();
            animation.Update();
        }

        public override void Draw(SpriteBatch batch)
        {
            if (saved) return;
            Vector2 drawPos = new Vector2((int)collider.position.X + collider.origin.X, (int)collider.position.Y + collider.origin.Y);
            animation.Draw(batch, drawPos - new Vector2(2, -2), collider.origin, 0, 0);
        }
    }
}
