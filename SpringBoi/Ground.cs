﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpringBoi
{
    public class Ground : GameObject
    {
        public RotatedRectangle     collider;
        public Vector2              size;
        public Vector2              startPos, endPos, moveStart, moveGoal;
        public int                  stopTime, stopTimer;
        public float                moveSpeed;
        public Texture2D            texMain;
        public Texture2D            texBody;
        public Color                color;
        public bool                 moving;
        public Vector2              lastMove;

        public Ground(Texture2D tex, Texture2D body, Vector2 pos, Vector2 sz, Vector2 end, float move, int stop)
        {
            position    = pos;
            startPos    = pos;
            endPos      = end;
            moveStart   = pos;
            moveGoal    = end;
            moveSpeed   = move;
            stopTime    = stop;
            stopTimer   = stop;
            size        = sz;
            texMain     = tex;
            texBody     = body;
            color       = Color.LightGreen;
            moving      = true;

            collider    = new RotatedRectangle(pos, sz, 0);
            collider.angle      = 0;
            collider.position   = position;
            collider.Update();
        }

        public Ground(Texture2D tex, Texture2D body, Vector2 pos, Vector2 sz)
        {
            position    = pos;
            size        = sz;
            texMain     = tex;
            texBody     = body;
            color       = Color.White;
            moving      = false;

            collider            = new RotatedRectangle(pos, sz, 0);
            collider.angle      = 0;
            collider.position   = position;
            collider.Update();
        }

        public override void Update()
        {
            if (moving) { Move(); }
            collider.position = position;
            collider.Update();
        }

        public void Move()
        {
            if (stopTimer > 0) --stopTimer;
            else
            {
                Vector2 delta = moveGoal - moveStart;

                if (delta != Vector2.Zero) delta.Normalize();
                delta *= moveSpeed;

                lastMove = delta;
                position += delta;

                float dist = Vector2.Distance(moveStart, moveGoal);
                float current = Vector2.Distance(position, moveGoal);
                float percentage = 1 - (current / dist);
                float accuracy = 1;

                if ((Math.Abs(delta.X) > 0.6f && Math.Abs(delta.X) < 0.8f) || (Math.Abs(delta.Y) > 0.6f && Math.Abs(delta.Y) < 0.8f)
                    || (moveGoal.X > moveStart.X && position.X >= moveGoal.X) || (moveGoal.X < moveStart.X && position.X <= moveGoal.X))
                { accuracy = .99f; }

                if (percentage >= accuracy)
                {
                    lastMove = Vector2.Zero;
                    position = moveGoal;
                    if (moveGoal == endPos)
                    {
                        moveStart = endPos;
                        moveGoal  = startPos;
                    }
                    else
                    {
                        moveStart = startPos;
                        moveGoal  = endPos;
                    }
                    stopTimer = stopTime;
                }
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            Rectangle source = new Rectangle(0, 0, (int)size.X, size.Y > 16 ? 16 : (int)size.Y);

            batch.Draw(texMain, new Vector2((collider.position.X), (collider.position.Y)),
                source, color, 0, Vector2.Zero, 1, SpriteEffects.None, 0f);

            if (size.Y <= 16)
                return;

            source = new Rectangle(0, 0, (int)size.X, (int)size.Y);
            batch.Draw(texBody, new Vector2((collider.position.X), (collider.position.Y) + 16),
                source, color, 0, Vector2.Zero, 1, SpriteEffects.None, 0.01f);
        }

        public override void Reset()
        {
            if (moving)
            {
                lastMove    = Vector2.Zero;
                position    = startPos;
                moveStart   = startPos;
                moveGoal    = endPos;
                stopTimer   = stopTime;
            }
        }
    }
}
