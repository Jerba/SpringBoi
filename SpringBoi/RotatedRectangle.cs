﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace SpringBoi
{
    public class RotatedRectangle
    {
        public List<Vector2>    vertices    = new List<Vector2>();
        public List<Vector2>    normals     = new List<Vector2>();
        public List<Vector2>    normalsAB   = new List<Vector2>();
        public Vector2          position;
        public Vector2          size;
        public Vector2          origin;
        public Rectangle        rect;
        public float            angle;
        public bool             flag;

        public RotatedRectangle(Vector2 pos, Vector2 sz, float rot = 0)
        {
            position    = pos;
            size        = sz;
            origin      = size / 2.0f;
            angle       = rot;
            rect        = new Rectangle(position.ToPoint(), size.ToPoint());
            vertices    = new List<Vector2>();
            normals     = new List<Vector2>();
            normalsAB   = new List<Vector2>();
        }

        public void Update()
        {
            vertices.Clear();
            normals.Clear();
            normalsAB.Clear();

            CalculateVertices();
            CalculateNormals();
        }
       
        private void CalculateVertices()
        {
            Vector2 orig = new Vector2(position.X + origin.X, position.Y + origin.Y);

            vertices.Add(RotatePoint(new Vector2(position.X, position.Y), orig, angle));
            vertices.Add(RotatePoint(new Vector2(position.X + rect.Width, position.Y), orig, angle));
            vertices.Add(RotatePoint(new Vector2(position.X + rect.Width, position.Y + rect.Height), orig, angle));
            vertices.Add(RotatePoint(new Vector2(position.X, position.Y + rect.Height), orig, angle));
        }

        private void CalculateNormals()
        {
            for (int i = 0; i < 4; i++)
            {
                Vector2 perp;
                if (i == 3) perp    = (Vector2.Subtract(vertices[0], vertices[i]));
                else        perp    = (Vector2.Subtract(vertices[i + 1], vertices[i]));
                normals.Add(Vector2.Normalize(new Vector2(perp.Y, perp.X * -1)));
            }
        }

        public bool Collision(RotatedRectangle rotatedRectangle)
        {
            return Collision(rotatedRectangle.normals,
                 rotatedRectangle.vertices);
        }

        public bool Collision(List<Vector2> bNormals, List<Vector2> bVertices)
        {
            foreach (Vector2 vec in normals) normalsAB.Add(vec);
            foreach (Vector2 vec in bNormals) normalsAB.Add(vec);

            flag = true;
            CalculateDot(bVertices);

            return flag;
        }

        private void CalculateDot(List<Vector2> bVertices)
        {
            foreach (Vector2 normal in normalsAB)
            {
                float dot = Vector2.Dot(normal, vertices[0]);
                float aMin = dot;
                float aMax = dot;

                dot = Vector2.Dot(normal, bVertices[0]);
                float bMin = dot;
                float bMax = dot;

                for (int i = 0; i < 4; ++i)
                {
                    dot = Vector2.Dot(normal, vertices[i]);
                    if (dot < aMin) aMin = dot;
                    if (dot > aMax) aMax = dot;

                    dot = Vector2.Dot(normal, bVertices[i]);
                    if (dot < bMin) bMin = dot;
                    if (dot > bMax) bMax = dot;
                }

                if (bMax >= aMax && aMax < bMin)
                {
                    flag = false;
                    break;
                }
                else if (aMax >= bMax && bMax < aMin)
                {
                    flag = false;
                    break;
                }
            }
        }

        private Vector2 RotatePoint(Vector2 point, Vector2 origin, float rotation)
        {
            Vector2 rotated = new Vector2
                ((float)(origin.X + (point.X - origin.X)
                     * Math.Cos(rotation) - (point.Y - origin.Y)
                     * Math.Sin(rotation)),
                (float)(origin.Y + (point.Y - origin.Y)
                     * Math.Cos(rotation) + (point.X - origin.X)
                     * Math.Sin(rotation)));
            return rotated;
        }
    }
}
