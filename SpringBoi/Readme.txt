SpringBoi

Credits
Made by Jerba in 2018 for Spring Game Jam held at KUAS, using MonoGame 3.6
Design, Programming, Art and Sound Effects by Jerba.
Sound Effects generated with https://jfxr.frozenfractal.com/
Font: Sunny Spring, more info in "Sunny Spring Font.txt"
Music: The Four Seasons (Spring), more info in "Music.txt"

About Game
Spring Boi is a simple collect-everything-to-get-to-next-level type game.
As it is a Game Jam game and I worked solo, it only has handful of levels.
Because the Player character is a spring, it is always Jumping.

"Story"
Spring Kids got lost and can't find their way to home anymore.
It is Spring Boi's Quest to Find and Save the Spring Kids!

Controls:
A / D   - Turn Spring Boi
W       - Hold to Jump higher
S       - Hold to do smaller Jumps
Arrows  - Same as WASD
Escape  - Exit game
F5      - Toggle Fullscreen (fullscreen is not handled properly)

Cheats:
Hold Left Shift and press
  Numbers - Skip to Level
  Left MB - Move Player to position


Building on Windows 7/10:
1. Have Visual Studio 2015 installed (2017 should work, but not tested by me)
2. Download and Install MonoGame 3.6
3. Download and Install Sunny Spring Font, link in "Sunny Spring Font.txt"
    - You could also set .spritefont to use some other font
4. Open up the solution in VS2015
5. Build the Content in MonoGame Content Pipeline Tool
    - Open Content/Content.mgcb
    - If assets are missing, add them (.png, .wav, .mp3 and .spritefont files)
    - Check that .wav file Processor is set as Sound Effect and .mp3 as Song
    - Press Build or F6 to build the assets
6. Use Visual Studio to Build & Run the project