﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpringBoi
{
    public class SheetAnimation
    {
        public Texture2D texture;
        public int width, height;
        public int numFrames, frameTime;
        public int curTime, curFrame;
        public bool playing;

        public void SetFrame(int frame)
        {
            if (frame > numFrames)
                frame   = numFrames - 1;
            curFrame    = frame;
            curTime     = 0;
        }

        public void Start() { playing = true; }
        public void Stop() { playing = false; }

        public SheetAnimation(Texture2D tex, int w, int h, int frames, int time)
        {
            texture     = tex;
            width       = w;
            height      = h;
            numFrames   = frames;
            frameTime   = time;
            curTime     = curFrame = 0;
            playing     = true;
        }

        public void Update()
        {
            if (!playing)
                return;

            curTime++;
            if (curTime >= frameTime)
            {
                curFrame++;
                if (curFrame > numFrames)
                    curFrame = 0;
                curTime = 0;
            }
        }

        public void Draw(SpriteBatch batch, Vector2 pos, Vector2 orig, float rot, SpriteEffects flip)
        {
            Rectangle source = new Rectangle(curFrame * width, 0, width, height);
            batch.Draw(texture, pos, source, Color.White, rot, orig, 1, flip, 1);
        }
    }
}
